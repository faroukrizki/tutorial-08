package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.service.PesertaService;
import com.example.service.ProdiService;

@RestController
public class RestControllerProdi {

	@Autowired
	ProdiService prodiDAO;
	
	@Autowired
	PesertaService pesertaDAO;
	
	
	@RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
	public ProdiModel displayDetailProdi(Model model, @RequestParam(value="kode", required = false) String kode_prodi){
		ProdiModel prodi = prodiDAO.selectProdi(kode_prodi);
		if(prodi!=null){
			PesertaModel oldest= prodi.getParticipants().get(0);
			PesertaModel youngest = prodi.getParticipants().get(prodi.getParticipants().size()-1);
			Integer oldestAge = pesertaDAO.calculateAge(oldest.getTgl_lahir());
			Integer youngestAge = pesertaDAO.calculateAge(youngest.getTgl_lahir());
			model.addAttribute("prodi", prodi);
			model.addAttribute("oldest", oldest);
			model.addAttribute("youngest", youngest);
			model.addAttribute("oldestAge", oldestAge);
			model.addAttribute("youngestAge", youngestAge);
			return prodi;
		}
		model.addAttribute("kode", kode_prodi);
		return prodi;
	}
}
