package com.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.model.PesertaModel;
import com.example.service.PesertaService;

@RestController
public class RestControllerPeserta {

	@Autowired
	PesertaService pesertaDAO;
	
	@RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
	public PesertaModel displayDetailPeserta(Model model, @RequestParam(value="nomor", required = false) String nomor){
		PesertaModel peserta = pesertaDAO.selectPeserta(nomor);
		if(peserta!=null){
			Integer usia = pesertaDAO.calculateAge(peserta.getTgl_lahir());
			model.addAttribute("peserta", peserta);
			model.addAttribute("usia", usia);
			return peserta;
		}
		model.addAttribute("nomor", nomor);
		return peserta;
	}
}
