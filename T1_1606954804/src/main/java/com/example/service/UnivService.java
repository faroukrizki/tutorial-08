package com.example.service;

import java.util.List;

import com.example.model.UnivModel;

public interface UnivService {
	UnivModel selectUniv(String kode_univ);
	List<UnivModel> selectAllUniv();
	List<UnivModel> selectAllUnivLimited(Integer offset, Integer limit);
}
