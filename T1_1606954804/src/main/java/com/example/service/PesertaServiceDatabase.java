package com.example.service;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dao.PesertaMapper;
import com.example.model.PesertaModel;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PesertaServiceDatabase implements PesertaService {

	@Autowired
	private PesertaMapper pesertaMapper;
	
	@Override
	public PesertaModel selectPeserta(String nomor) {
		log.info("select peserta with nomor peserta {}", nomor);
		return pesertaMapper.selectPeserta(nomor);
	}

	@Override
	public List<PesertaModel> selectAllPeserta() {
		log.info("select every peserta");
		return pesertaMapper.selectAllPeserta();
	}

	@Override
	public Integer calculateAge(Date birthDate) {
		if(birthDate!=null){
			LocalDate birth = new java.sql.Date(birthDate.getTime()).toLocalDate();
			LocalDate now = new java.sql.Date(new Date().getTime()).toLocalDate();
			return Period.between(birth, now).getYears();
		}
		return 0;
	}
	
	
}
