package com.example.dao;

import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.example.model.PesertaModel;
import com.example.model.ProdiModel;
import com.example.model.UnivModel;

import java.util.List;

@Mapper
public interface ProdiMapper {
	@Select("select * from prodi where kode_prodi=#{kode_prodi}")
	@Results(value={
			@Result(property="kode_prodi", column="kode_prodi"),
			@Result(property="nama_prodi", column="nama_prodi"),
			@Result(property="universitas", column="kode_univ", one=@One(select = "selectUniversitas")),
			@Result(property="participants", column="kode_prodi", javaType=List.class, many=@Many(select="selectParticipants"))
	})
	ProdiModel selectProdi(@Param("kode_prodi") String kode_prodi);
	
	@Select("select * from prodi")
	@Results(value={
			@Result(property="kode_prodi", column="kode_prodi"),
			@Result(property="nama_prodi", column="nama_prodi"),
			@Result(property="universitas", column="kode_univ", one=@One(select = "selectUniversitas")),
			@Result(property="participants", column="kode_prodi", javaType=List.class, many=@Many(select="selectParticipants"))
	})
	List<ProdiModel> selectAllProdi();
	
	@Select("select * from univ where kode_univ=#{kode_univ}")
	UnivModel selectUniversitas(@Param("kode_univ") String kode_univ);
	
	@Select("select * from peserta where kode_prodi = #{kode_prodi} order by tgl_lahir asc")
	List<PesertaModel> selectParticipants(@Param("kode_prodi") String kode_prodi);
}
